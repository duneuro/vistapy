#include <Python.h>

#include <iostream>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vistaio/vistaio.h>

namespace py = pybind11;

struct VistaPyException : public std::exception {
  std::string msg;
  explicit VistaPyException(const std::string& m) : msg(m) {}
  const char* what() const noexcept override { return msg.c_str(); }
};

namespace VistaIODetail {
// renumber the vertex according to the dune numbering (tensor product)
static unsigned int renumber(unsigned int dim, unsigned int count,
                             unsigned int i) {
  if (dim == 2) {
    if (count == 4) {
      static unsigned int indices[] = {0, 1, 3, 2};
      return indices[i];
    }
  } else if (dim == 3) {
    if (count == 8) {
      static unsigned int indices[] = {0, 1, 3, 2, 4, 5, 7, 6};
      return indices[i];
    }
  }
  return i;
}
/**
 * @brief read a single string from the position
 */
std::string readString(VistaIOAttrListPosn* posn) {
  VistaIOString str;
  VistaIOGetAttrValue(posn, NULL, VistaIOStringRepn, &str);
  return std::string(str);
}

// representation of a primitive node
template <class T>
struct PrimitiveNode {
  VistaIONodeBaseRec base;
  T count;
  T vertexIndices[];
};
}

// print out a single attribute
void printAttributeInfo(const std::string& prefix, VistaIOAttrListPosn* posn) {
  std::cout << prefix << " name: " << std::string(VistaIOGetAttrName(posn))
            << std::endl;
  std::cout << prefix << " repr: " << VistaIOGetAttrRepn(posn) << std::endl;
}

// print out the attributes of the given graph
void printGraphAttributes(const std::string& prefix, VistaIOGraph graph) {
  VistaIOAttrListPosn posn_prim;
  for (VistaIOFirstAttr(VistaIOGraphAttrList(graph), &posn_prim);
       VistaIOAttrExists(&posn_prim); VistaIONextAttr(&posn_prim)) {
    printAttributeInfo(prefix, &posn_prim);
  }
}

// read the mesh nodes
std::vector<std::array<double, 3>> readNodes(VistaIOGraph graph) {
  std::vector<std::array<double, 3>> result;
  // read vertex graph
  for (unsigned int i = 1; i <= graph->size; ++i) {
    VistaIONode node = VistaIOGraphGetNode(graph, i);
    VistaIOFloat* pvfNode = reinterpret_cast<VistaIOFloat*>(node->data);
    std::array<double, 3> current;
    for (unsigned int j = 0; j < 3; ++j) current[j] = pvfNode[j + 1];
    result.push_back(current);
  }
  return result;
}

// read mesh elements. for cubes, nodes are renumbered according to the dune
// numbering
std::vector<std::vector<std::size_t>> readElements(VistaIOGraph graph) {
  std::vector<std::vector<std::size_t>> result;
  for (VistaIONode node =
           static_cast<VistaIONode>(VistaIOGraphFirstNode(graph));
       node; node = static_cast<VistaIONode>(VistaIOGraphNextNode(graph))) {
    auto* pnode =
        reinterpret_cast<VistaIODetail::PrimitiveNode<VistaIOLong>*>(node);
    std::vector<std::size_t> element(pnode->count);
    for (unsigned int i = 0; i < element.size(); ++i) {
      element[i] =
          pnode->vertexIndices[VistaIODetail::renumber(3, pnode->count, i)] - 1;
    }
    result.push_back(element);
  }
  return result;
}

// extract conductivity tensors, stored in simbio format as
// xx yy zz xy xz yz
std::vector<std::array<double, 6>> readTensors(VistaIOImage image) {
  std::vector<std::array<double, 6>> result;
  for (unsigned int i = 0; i < image->ncolumns; ++i) {
    std::array<double, 6> current;
    for (unsigned int j = 0; j < image->nbands; ++j) {
      current[j] = VistaIOGetPixel(image, j, 0, i);
    }
    result.push_back(current);
  }
  return result;
}

// extract material properties
std::vector<double> readMatProps(VistaIOImage image) {
  std::vector<double> result;
  for (unsigned int i = 0; i < image->ncolumns; ++i) {
    result.push_back(VistaIOGetPixel(image, 0, 0, i));
  }
  return result;
}

struct Mesh {
  // mesh nodes
  std::vector<std::array<double, 3>> nodes;
  // mesh elements
  std::vector<std::vector<std::size_t>> elements;
  // conductivity tensors
  std::vector<std::array<double, 6>> tensors;
  // element labels
  std::vector<double> matprops;
};

Mesh read_vista_mesh(const std::string& filename) {
  const int dim = 3;
  FILE* f = fopen(filename.c_str(), "rb");
  if (!f) {
    throw VistaPyException("Can not open filename");
  }
  VistaIOAttrList list = VistaIOReadFile(f, NULL);
  fclose(f);
  std::size_t vertices = 0;
  std::size_t elements = 0;

  Mesh mesh;

  // loop through all attributes
  VistaIOAttrListPosn posn;
  for (VistaIOFirstAttr(list, &posn); VistaIOAttrExists(&posn);
       VistaIONextAttr(&posn)) {
    printAttributeInfo("root attribute", &posn);
    // if its a graph, it might contain the mesh information
    if (VistaIOGetAttrRepn(&posn) == VistaIOGraphRepn) {
      // extract the component interpretation
      VistaIOGraph graph = NULL;
      VistaIOGetAttrValue(&posn, NULL, VistaIOGraphRepn, &graph);
      VistaIOAttrListPosn posComponentInterp;
      assert(VistaIOLookupAttr(VistaIOGraphAttrList(graph), "component_interp",
                               &posComponentInterp));
      std::string componentInterp =
          VistaIODetail::readString(&posComponentInterp);
      printGraphAttributes(componentInterp, graph);
      // the mesh nodes are stored in the vertex part, elements, tensors and
      // labels are primitives
      if (componentInterp == "vertex") {
        mesh.nodes = readNodes(graph);
      } else if (componentInterp == "primitive") {
        mesh.elements = readElements(graph);
        VistaIOAttrListPosn posCondTensor;
        if (VistaIOLookupAttr(VistaIOGraphAttrList(graph), "condtensor",
                              &posCondTensor)) {
          VistaIOImage image;
          VistaIOGetAttrValue(&posCondTensor, NULL, VistaIOImageRepn, &image);
          mesh.tensors = readTensors(image);
        }
        VistaIOAttrListPosn posMatProps;
        if (VistaIOLookupAttr(VistaIOGraphAttrList(graph), "matprops",
                              &posMatProps)) {
          VistaIOImage image;
          VistaIOGetAttrValue(&posMatProps, NULL, VistaIOImageRepn, &image);
          mesh.matprops = readMatProps(image);
        }
      }
    }
  }
  return mesh;
}

PYBIND11_MODULE(vistapy, m) {
  m.doc() = "vistapy library";

  m.def("read_vista_mesh", &read_vista_mesh);
  py::class_<Mesh>(m, "Mesh")
      .def_readwrite("nodes", &Mesh::nodes)
      .def_readwrite("elements", &Mesh::elements)
      .def_readwrite("tensors", &Mesh::tensors)
      .def_readwrite("matprops", &Mesh::matprops);
}
