### vistapy

A small python module to read meshes stored in the vista format by vgrid.
It reads nodes, elements, labels and tensors.
Cubes are renumbered to conform to the Dune numbering.

## license

vistapy is published under the terms of the MIT license.

## prerequisites
- libvistaio-dev

## example

```python
import vistapy as vp
import numpy as np

m = vp.read_vista_mesh('/path/to/your/vista/file.v')

nodes = np.array(m.nodes, dtype=float)
elements = np.array(m.elements, dtype=int)
tensors = np.array(m.tensors, dtype=float)
labels = np.array(m.matprops, dtype=int)
```
