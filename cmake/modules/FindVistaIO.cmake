# Module that checks whether VistaIO is available
#
# Accepts the following input variable
# VISTAIO_PREFIX: Prefix under which VistaIO is installed
# VISTAIO_INCLUDE_DIR: Include directories for VistaIO
# VISTAIO_LIBRARY: Full path to VistaIO library
#
# The following variable will be set:
# VISTAIO_FOUND: whether VistaIO is available
# VISTAIO_INCLUDE_DIRS: Include directories for VistaIO
# VISTAIO_LIBRARIES: Full path to libraries needed to link
#   to VistaIO
#

find_package(PkgConfig)
pkg_check_modules(PC_VISTAIO QUIET libvistaio)

find_path(VISTAIO_INCLUDE_DIR vistaio/vistaio.h
  HINTS ${PC_VISTAIO_INCLUDEDIR} ${PC_VISTAIO_INCLUDE_DIRS} ${VISTAIO_PREFIX}
  PATH_SUFFIXES vistaio-1.2)

find_library(VISTAIO_LIBRARY vistaio
  HINTS ${PC_VISTAIO_LIBDIR} ${PC_VISTAIO_INCLUDE_DIRS})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args("VistaIO" DEFAULT_MSG
  VISTAIO_LIBRARY VISTAIO_INCLUDE_DIR)

mark_as_advanced(VISTAIO_INCLUDE_DIR VISTAIO_LIBRARY)

if (VISTAIO_FOUND)
    set(VISTAIO_LIBRARIES ${VISTAIO_LIBRARY})
    set(VISTAIO_INCLUDE_DIRS ${VISTAIO_INCLUDE_DIR})
    file(APPEND ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeOutput.log
        "Determining location of VistaIO succeeded:\n"
        "Include directory: ${VISTAIO_INCLUDE_DIRS}\n"
        "Library directory: ${VISTAIO_LIBRARIES}\n")
else(VISTAIO_FOUND)
    file(APPEND ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeError.log
        "Determining location of VistaIO failed:\n"
        "Include directory: ${VISTAIO_INCLUDE_DIR}\n"
        "Library directory: ${VISTAIO_LIBRARY}\n")
endif(VISTAIO_FOUND)

set(HAVE_VISTAIO ${VISTAIO_FOUND})
